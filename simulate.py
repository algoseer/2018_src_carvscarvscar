import numpy as np
from pylab import plot, ion, show, pause, cla, xlim, ylim, text
from numpy.linalg import norm

N=4
cars = np.random.rand(N,2)
move_dir = np.zeros(cars.shape)

ion()

l=0.5
while True:
	for i in range(N):
		text(cars[i,0],cars[i,1],str(i+1))

		this_car = cars[i]

		next_car = cars[(i+1)%N]
		prev_car = cars[(i-1)]

		
		move_dir[i] = l*(this_car-prev_car)/norm(this_car-prev_car)+(1-l)*(next_car - this_car)/norm(next_car-this_car)
#		r = np.random.randn() * 0.1
#		move_dir[i] += r*(this_car-prev_car)/norm(this_car-prev_car)-r*(next_car - this_car)/norm(next_car-this_car)


		mag = np.random.rand() *0.1
		move_dir[i] *= mag
		

	cars +=move_dir
	
	xlim([-2,2])
	ylim([-2,2])
	pause(0.2)
	cla()
